package com.newflypig.jblog.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;

import com.newflypig.jblog.dao.IBaseDAO;
import com.newflypig.jblog.dao.ILianyihuiDAO;
import com.newflypig.jblog.model.Lianyihui;
import com.newflypig.jblog.service.ILianyihuiService;

@Service("lianyihuiService")
public class LianyihuiServiceImpl extends BaseServiceImpl<Lianyihui> implements ILianyihuiService{
	
	@Resource(name = "lianyihuiDao")
	private ILianyihuiDAO lianyihuiDao;
	
	@Override
	protected IBaseDAO<Lianyihui> getDao() {
		return this.lianyihuiDao;
	}

	@Override
	public List<Lianyihui> findForShow() {
		ProjectionList pList = Projections.projectionList();  
		pList.add(Projections.property("lianyihui.realname").as("realname"));  
		pList.add(Projections.property("lianyihui.phonenumber").as("phonenumber"));
		pList.add(Projections.property("lianyihui.remark").as("remark"));
		
		return this.lianyihuiDao.findByDC(
			DetachedCriteria.forClass(Lianyihui.class, "lianyihui")
			.setProjection(pList)
			.add(Restrictions.isNotNull("lianyihui.showid"))
			.addOrder(Order.asc("lianyihui.showid"))
			.setResultTransformer(Transformers.aliasToBean(Lianyihui.class))
		);
	}

	@Override
	public Lianyihui findByHash(String hash) {
		if(hash == null || hash.length() != 10)
			return null;
		
		ProjectionList pList = Projections.projectionList();  
		pList.add(Projections.property("lianyihui.id").as("id"));
		pList.add(Projections.property("lianyihui.realname").as("realname"));  
		pList.add(Projections.property("lianyihui.phonenumber").as("phonenumber"));
		pList.add(Projections.property("lianyihui.remark").as("remark"));
		
		List<Lianyihui> list = this.lianyihuiDao.findByDC(
			DetachedCriteria.forClass(Lianyihui.class, "lianyihui")
			.setProjection(pList)
			.add(Restrictions.eq("lianyihui.hash", hash))
			.setResultTransformer(Transformers.aliasToBean(Lianyihui.class))
		);
		
		if(list.isEmpty())
			return null;
		
		return list.get(0);
	}

}
