package com.newflypig.jblog.service;

import java.util.List;

import com.newflypig.jblog.exception.JblogException;
import com.newflypig.jblog.model.Article;
import com.newflypig.jblog.model.Pager;

/**
 *	定义关于Article类的特别服务方法
 *	time：2015年11月28日
 *
 */
public interface IArticleService extends IBaseService<Article>{
	/**
	 * 倒序排列所有文章
	 * @return
	 */
	public List<Article> findAllDesc();
	
	/**
	 * 通过归档ID查找文章
	 * @param id
	 * @return
	 */
	public List<Article> findByArchiveId(Integer id);
	
	/**
	 * 获取Widget小插件
	 * @return
	 */
	public List<Article> findWidgets();
	
	/**
	 * 根据Menu获取Article分页模型，正式文章
	 * @param page
	 * @return
	 */
	public Pager<Article> findPagerByMenu(Integer page, String menuName);
	
	
	
	/**
	 * 根据Tag获取Article分页模型，正式文章
	 * @param page
	 * @param tagUrlName
	 * @return
	 */
	public Pager<Article> findPagerByTag(Integer page, String tagUrlName);
	
	/**
	 * 根据Archive获取Article分页模型，正式文章
	 * @param page
	 * @param archiveUrlName
	 * @return
	 */
	public Pager<Article> findPagerByArchive(Integer page, String archiveUrlName); 
	
	
	/**
	 * 根据Menu及Head|Foot获取页面文字块
	 * 本函数所返回的Article仅仅包含html属性
	 * @param typeArchiveFoot
	 * @param mENU_INDEX_URL_NAME
	 * @return
	 */
	public List<Article> findHFArticlesByMenu(short typeArchiveFoot, String mENU_INDEX_URL_NAME);
	
	/**
	 * 根据urlName 或者 articleId 获取单个article
	 * @param articleUNorId
	 * @return
	 */
	public Article findByUNorId(String articleUNorId);
	
	/**
	 * 为后台寻找所有状态为发布或编辑的Articles
	 * @param page
	 * @return
	 */
	public Pager<Article> findPagerForAdmin(Integer page);
	
	/**
	 * 为后台寻找所有状态为删除的Articles
	 * @param page
	 * @return
	 */
	public Pager<Article> findPagerForAdminTrashBox(Integer page);
	
	/**
	 * 永久删除文章
	 * @param articleId
	 */
	public void deleteForeverById(Integer articleId);
	
	/**
	 * 使文章的点击量rate自增1
	 */
	public void ratePP(Integer articleId);
	
	/**
	 * 恢复一片文章，更新其state为publish
	 * @param articleId
	 */
	public void recovery(Integer articleId);
	
	/**
	 * 从前台完整的新建一个Article
	 * @param article
	 * @return 添加后新article的主键Id
	 * @throws JblogException 
	 */
	public Integer add(Article article);
	
	/**
	 * 为sitemap提供一个文章url列表，只需要简单的urlName或者id
	 * @return
	 */
	public List<String> findArticleUrls();
	
	/**
	 * 执行一个无返回集的存储过程
	 * @param string
	 */
	public void callNoResultableProc(String string);
	
	/**
	 * 为RSS提供文章数据源，倒序，30篇，包含:
	 * id,urlName,title,html,createDate,tags
	 * @return
	 */
	public List<Article> findAllForRSS();
	
	/**
	 * 为「日志列表」提供文章数据源，倒序，全部，showInList属性为真，包含：
	 * id,urlName,title,createDate
	 * @return
	 */
	public List<Article> findAllForList();
	
	//交换两个widget的index
	public void exchangeIndex(Integer theId, Integer otherId);
}
