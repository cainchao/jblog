package com.newflypig.jblog.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.geetest.sdk.java.GeetestLib;
import com.newflypig.jblog.model.Article;
import com.newflypig.jblog.model.BlogCommon;
import com.newflypig.jblog.model.BlogConfig;
import com.newflypig.jblog.model.Result;
import com.newflypig.jblog.service.IArticleService;
import com.newflypig.jblog.service.IBlogService;
import com.newflypig.jblog.service.ISystemService;
import com.newflypig.jblog.service.ITagService;
import com.newflypig.jblog.util.BlogUtils;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
	
	@Autowired
	private ISystemService systemService;
	
	@Autowired
	private IBlogService blogService;
	
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private ITagService tagService;
	
	@Autowired
	private BlogCommon blogCommon;
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public ModelAndView main(){
		return new ModelAndView("/admin/main");
	}
	
	@RequestMapping(value = "/duoshuo/{command}", method = RequestMethod.GET)
	public ModelAndView duoshuo(@PathVariable String command){
		ModelAndView mav = new ModelAndView("/admin/duoshuo");
		mav.addObject("command", command);
		mav.addObject("duoshuoId", BlogConfig.DUOSHUO_ID);
		return mav;
	}
	
	@RequestMapping(value = "/settings/statics", method = RequestMethod.GET)
	public ModelAndView statics(){
		return new ModelAndView("/admin/statics");
	}
	
	@RequestMapping(value = "/settings/statics", method = RequestMethod.POST)
	public ModelAndView saveStatics(String key,String value, HttpSession session){
		value = BlogUtils.deHtml(value);
		this.systemService.update(key,value);
		this.blogCommon.setKeyValue(key,value);
		return new ModelAndView("ajaxResult","result",new Result());
	}
	
	@RequestMapping(value = "/settings/statics/update", method = RequestMethod.POST)
	public ModelAndView update(){
		Result result = new Result();
		try{
			this.blogService.updateStatics();
		}catch(Exception e){
			result.setCode(Result.FAIL);
			result.setMessage(e.getMessage());
		}
		return new ModelAndView("ajaxResult", "result", result);
	}
	
	@RequestMapping(value = "/settings/account", method = RequestMethod.GET)
	public ModelAndView accountData(){
		ModelAndView mav = new ModelAndView("/admin/account");
		
		return mav;
	}
	
	@RequestMapping(value = "/account/updatePwd", method = RequestMethod.POST)
	public ModelAndView changePwd(String newPwd, String oldPwd, String geetest_challenge, String geetest_validate, String geetest_seccode){
		Result result = new Result();
        
		GeetestLib gtSdk = new GeetestLib(BlogConfig.GEETEST_AK, BlogConfig.GEETEST_SK);
        
        String challenge = geetest_challenge;
        String validate  = geetest_validate;
        String seccode   = geetest_seccode;
        
        if( gtSdk.enhencedValidateRequest(challenge, validate, seccode) == 0 ){
        	result.setCode(Result.FAIL);
        	result.setMessage("人机识别失败，请检查您的操作行为。");
        	return new ModelAndView("ajaxResult", "result", result);
        }
        
        if( this.systemService.updatePwd(oldPwd, newPwd) == 0){
        	result.setCode(Result.FAIL);
        	result.setMessage("原始密码错误，无法修改为新密码。");
        	return new ModelAndView("ajaxResult", "result", result);
        }
        
		return new ModelAndView("ajaxResult", "result", result);
	}
	
	@RequestMapping(value = "/account/update", method = RequestMethod.POST)
	public ModelAndView accountUpdate(String key,String value){
		value = BlogUtils.deHtml(value);
		this.systemService.update(key,value);
		BlogConfig.setKeyValue(key,value);
		return new ModelAndView("ajaxResult","result",new Result());
	}
	
	@RequestMapping(value = "/widgets", method = RequestMethod.GET)
	public ModelAndView widgets(){
		List<Article> widgets = this.articleService.findWidgets();
		for(Article a: widgets){
			a.setHtml(BlogUtils.html(a.getHtml()));
			a.setMarkdown(BlogUtils.html(a.getMarkdown()));
		}
		return new ModelAndView("/admin/widgets", "widgets", widgets);
	}
	
	@RequestMapping(value = "/addOrUpdateWidget", method = RequestMethod.POST)
	public ModelAndView addOrUpdateWidget(Article article){
		Result result = new Result();
		
		article.setType(Article.TYPE_WIDGET);
		article.setState(Article.STATE_PUBLISH);
		if(article.getArticleId() == 0){
			//新增
			article.setArticleId(null);
			Integer newArticleId = this.articleService.add(article);
			result.setData("{\"articleId\": " + newArticleId + "}");
		}else{
			//修改
			this.articleService.update(article);
		}
		blogCommon.setWidgetList(this.articleService.findWidgets());
		
		return new ModelAndView("ajaxResult", "result", result);
	}
	
	@RequestMapping(value = "/deleteWidget", method = RequestMethod.POST)
	public ModelAndView deleteWidget(Integer articleId){
		this.articleService.deleteForeverById(articleId);
		blogCommon.setWidgetList(this.articleService.findWidgets());
		return new ModelAndView("ajaxResult", "result", new Result());
	}
	
	@RequestMapping(value = "/widgets/move", method = RequestMethod.POST)
	public ModelAndView widgetIndexExchange(Integer theId, Integer otherId){
		this.articleService.exchangeIndex(theId,otherId);
		return new ModelAndView("ajaxResult", "result", new Result());
	}
}
