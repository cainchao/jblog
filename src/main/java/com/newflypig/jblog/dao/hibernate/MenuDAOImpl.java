package com.newflypig.jblog.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.newflypig.jblog.dao.IMenuDAO;
import com.newflypig.jblog.model.Menu;

@Repository("menuDao")
public class MenuDAOImpl extends BaseHibernateDAO<Menu> implements IMenuDAO {

}
