package com.newflypig.jblog.test;

import org.junit.Test;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;

public class QiniuTest {
	
	@Test
	public void uploadTest(){
		UploadManager uploadManager = new UploadManager();
	    Auth auth = Auth.create("xxxx", "xxxx");
	    String token = auth.uploadToken("images");
	    try {
			@SuppressWarnings("unused")
			Response r = uploadManager.put("hello world".getBytes(), "yourkey", token);
		} catch (QiniuException e) {
			e.printStackTrace();
		}
	}
}
